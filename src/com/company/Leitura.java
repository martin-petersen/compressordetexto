package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Leitura {

    private ArrayList<Character> characterArray;
    private ArrayList<Integer> counterArray;

    public ArrayList<Character> getCharacterArray() {
        return characterArray;
    }

    public ArrayList<Integer> getCounterArray() {
        return counterArray;
    }

    public void setCharacterArray(ArrayList<Character> characterArray) {
        this.characterArray = characterArray;
    }

    public void setCounterArray(ArrayList<Integer> counterArray) {
        this.counterArray = counterArray;
    }

    public Leitura(String nomeArquivo) {
        ArrayList<Character> characterArrayList = new ArrayList<Character>();
        ArrayList<Integer> counterArrayList = new ArrayList<Integer>();

        try {
            FileReader rdr = new FileReader(nomeArquivo);
            BufferedReader brdr = new BufferedReader(rdr);
            char C;
            int cont = 0;

            String line = brdr.readLine();

            while (line != null) {

                for(int i=0; i<line.length(); ++i) {
                    C = line.charAt(i);

                    for(int j=0; j<characterArrayList.size(); ++j) {
                        if(C == characterArrayList.get(j)){
                            counterArrayList.set(j,1);
                            cont++;
                        }
                    }

                    if(cont != 0) {
                        cont = 0;
                    } else {
                        characterArrayList.add(C);
                        counterArrayList.add(1);
                    }
                }

                line = brdr.readLine();
            }

            rdr.close();
            setCharacterArray(characterArrayList);
            setCounterArray(counterArrayList);
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Leitura(String nomeArquivo, String path) {
        ArrayList<Character> characterArrayList = new ArrayList<Character>();
        ArrayList<Integer> counterArrayList = new ArrayList<Integer>();

        if(path.charAt(path.length()-1)!='/') {
            path = path.concat("/");
            path = path.concat(nomeArquivo);
        } else {
            path = path.concat(nomeArquivo);
        }

        try {
            FileReader rdr = new FileReader(path);
            BufferedReader brdr = new BufferedReader(rdr);
            char C;
            int cont = 0;

            String line = brdr.readLine();

            while (line != null) {

                for(int i=0; i<line.length(); ++i) {
                    C = line.charAt(i);

                    for(int j=0; j<characterArrayList.size(); ++j) {
                        if(C == characterArrayList.get(j)){
                            counterArrayList.set(j,counterArrayList.get(j)+1);
                            cont++;
                        }
                    }

                    if(cont != 0) {
                        cont = 0;
                    } else {
                        characterArrayList.add(C);
                        counterArrayList.add(1);
                    }
                }

                line = brdr.readLine();
            }

            rdr.close();
            setCharacterArray(characterArrayList);
            setCounterArray(counterArrayList);
        }

        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

