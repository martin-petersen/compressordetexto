package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Leitura reader = new Leitura("TextoSimples.txt", "/home/martin.rocha/Área de Trabalho");

        ArrayList<Character> teste = reader.getCharacterArray();
        ArrayList<Integer> teste2 = reader.getCounterArray();

        for(int i=0; i<teste.size(); ++i) {
            System.out.println("Na posição: " + i + " Caractere: " + teste.get(i) + " Num. Ocorrencias: " + teste2.get(i));
        }
    }
}
